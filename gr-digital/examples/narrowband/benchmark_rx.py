#!/usr/bin/env python
#
# Copyright 2010,2011,2013 Free Software Foundation, Inc.
# 
# This file is part of GNU Radio
# 
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 
from oml4py import OMLBase

import sys, os
import time

path = os.path.dirname(sys.argv[0]).split("share")[0] + "lib/python2.7/dist-packages"
sys.path.append(path)

os.environ['SHELL'] = "/bin/bash"
os.environ['LC_ALL'] = 'C'
os.environ['LANG'] = 'C'
os.environ['PYTHONPATH'] = os.path.dirname(sys.argv[0]).split("share")[0] +'lib/python2.7/dist-packages'
os.environ['PKG_CONFIG_PATH'] = os.path.dirname(sys.argv[0]).split("share")[0] +'lib/pkgconfig'


from gnuradio import gr, gru
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio.eng_option import eng_option
from optparse import OptionParser

# From gr-digital
from gnuradio import digital

# from current dir
from receive_path import receive_path
from uhd_interface import uhd_receiver

#uhd_transmitter and transmit_path imports
from transmit_path import transmit_path
from uhd_interface import uhd_transmitter

import struct
import sys
import socket

#import os
#print os.getpid()
#raw_input('Attach and press enter: ')

class my_top_block(gr.top_block):
    def __init__(self, modulator, demodulator, rx_callback, options):   # modulator value added
        gr.top_block.__init__(self)

	# /////////////////////////////////////////////////////////////////////////////
        # /----------------------------- uhd_transmitter -----------------------------/
        # /////////////////////////////////////////////////////////////////////////////
        #options.constellation_points = 2
        options.bitrate_tr = 249993.75
        if(options.tx_freq is not None):
            # Work-around to get the modulation's bits_per_symbol
            args = modulator.extract_kwargs_from_options(options)
            symbol_rate = options.bitrate_tr / modulator(**args).bits_per_symbol()
            
            self.sink = uhd_transmitter(options.args, symbol_rate,
                                        options.samples_per_symbol, options.tx_freq,
                                        options.lo_offset, options.tx_gain,
                                        options.spec, options.antenna,
                                        options.clock_source, options.verbose)
            options.samples_per_symbol = self.sink._sps
        
        elif(options.to_file is not None):
            sys.stderr.write(("Saving samples to '%s'.\n\n" % (options.to_file)))
            self.sink = blocks.file_sink(gr.sizeof_gr_complex, options.to_file)
        else:
            sys.stderr.write("No sink defined, dumping samples to null sink.\n\n")
            self.sink = blocks.null_sink(gr.sizeof_gr_complex)
    
        # do this after for any adjustments to the options that may
        # occur in the sinks (specifically the UHD sink)
        self.txpath = transmit_path(modulator, options)
        self.connect(self.txpath, self.sink)
        

        # //////////////////////////////////////////////////////////////////////////////
        # /------------------------------- uhd_receiver -------------------------------/
        # //////////////////////////////////////////////////////////////////////////////
	#options.constellation_points = 4
        options.bitrate_re = 333325

        if(options.rx_freq is not None):
            # Work-around to get the modulation's bits_per_symbol
            args = demodulator.extract_kwargs_from_options(options)
            symbol_rate = options.bitrate_re / demodulator(**args).bits_per_symbol()

            self.source = uhd_receiver(options.args, symbol_rate,
                                       options.samples_per_symbol, options.rx_freq, 
                                       options.lo_offset, options.rx_gain,
                                       options.spec, options.antenna,
                                       options.clock_source, options.verbose)
            options.samples_per_symbol = self.source._sps

        elif(options.from_file is not None):
            sys.stderr.write(("Reading samples from '%s'.\n\n" % (options.from_file)))
            self.source = blocks.file_source(gr.sizeof_gr_complex, options.from_file)
        else:
            sys.stderr.write("No source defined, pulling samples from null source.\n\n")
            self.source = blocks.null_source(gr.sizeof_gr_complex)

        # Set up receive path
        # do this after for any adjustments to the options that may
        # occur in the sinks (specifically the UHD sink)
        self.rxpath = receive_path(demodulator, rx_callback, options) 

        self.connect(self.source, self.rxpath)
        print >> sys.stderr, options


# /////////////////////////////////////////////////////////////////////////////
#                                   main
# /////////////////////////////////////////////////////////////////////////////

global n_rcvd, n_right

def main():
    global n_rcvd, n_right, new_channel, cur_channel,  tune_freq, tune_request, tb, last_ok_time, start_time, tune_flag, intr_status, intr_channel , channels_list 

    tune_flag = True
    n_rcvd = 0
    n_right = 0

     ########### List of avaliable pre-determined channels ############
    channels_list = [1598.875e6, 1599.4375e6 , 1600.0e6, 1600.5625e6, 1601.125e6]
    ##################################################################

    def send_pkt(payload='', eof=False):        # /----- RX-transmitter -----/
        return tb.txpath.send_pkt(payload, eof) # /--------------------------/

    #### new variables of rx_callback
    cur_channel = 0    ######## reception freq channel
    intr_channel = 3
    tune_request = False
    
    intr_status = False
    def rx_callback(ok, payload):
        global n_rcvd, n_right, cur_channel, tune_request, tune_freq, tb, tune_flag, intr_status, intr_channel , channels_list
        (pktno,) = struct.unpack('!H', payload[0:2])
	data_packet = payload[2:]
	#data = data_packet
	#data = data_packet[:-2]
#	interf_data = data_packet[-1:]   #####
#	data = interf_data[1:]

        n_rcvd += 1	

        if ok:
	    last_ok_time = time.time()
            n_right += 1
	    if options.server:
		sock.sendall(data_packet[:-1])


############################  Transmitter control packet ############################
	if ok  and (data_packet[-1:].isdigit()):
            if int(data_packet[-1:]) == 0 and cur_channel != int(data_packet[-1:]):
                print "tune packet '0'"
                cur_channel = int(data_packet[-1:])
#                uhd_receiver.set_freq(tb.source, channels_list[cur_channel], 0)
#                time.sleep(0.5)

            elif int(data_packet[-1:]) == 1 and cur_channel != int(data_packet[-1:]):
                print "tune packet '1'"
                cur_channel = int(data_packet[-1:])
#                uhd_receiver.set_freq(tb.source, channels_list[cur_channel], 0)
#                time.sleep(0.5)

            elif int(data_packet[-1:]) == 2 and cur_channel != int(data_packet[-1:]):
                print "tune packet '2'"
                cur_channel = int(data_packet[-1:])
#                uhd_receiver.set_freq(tb.source, channels_list[cur_channel], 0)
#                time.sleep(0.5)

            elif int(data_packet[-1:]) == 3 and cur_channel != int(data_packet[-1:]):
                print "tune packet '3'"
                cur_channel = int(data_packet[-1:])
#                uhd_receiver.set_freq(tb.source, channels_list[cur_channel], 0)
#                time.sleep(0.5)

            elif int(data_packet[-1:]) == 4 and cur_channel != int(data_packet[-1:]):
                print "tune packet '4'"
                cur_channel = int(data_packet[-1:])
#                uhd_receiver.set_freq(tb.source, channels_list[cur_channel], 0)
#                time.sleep(0.5)
	    

#	    print "interf_data[:1]" + str(interf_data[:1])
	
############################  Interferer control packet ############################
	    elif int(data_packet[-1:]) == 5 and intr_channel != int(data_packet[-1:]):
		print "Interferer ctr packet '0'"
#                intr_status = True
                intr_channel = int(data_packet[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)
	    elif int(data_packet[-1:]) == 6 and intr_channel != int(data_packet[-1:]):
	        print "Interferer ctr packet '1'"
#                intr_status = True
                intr_channel = int(data_packet[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)
	    elif int(data_packet[-1:]) == 7 and intr_channel != int(data_packet[-1:]):
		print "Interferer ctr packet '2'"
#                intr_status = True
                intr_channel = int(data_packet[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)
	    elif int(data_packet[-1:]) == 8 and intr_channel != int(data_packet[-1:]):	
		print "Interferer ctr packet '3'"
#                intr_status = True
                intr_channel = int(data_packet[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)
	    elif int(data_packet[-1:]) == 9 and intr_channel != int(data_packet[-1:]):	
		print "Interferer ctr packet '4'"
#                intr_status = True
                intr_channel = int(data_packet[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)
	    elif int(data_packet[-1:]) == -1 and intr_status == True:
		#intr_status = False
                print "Interferer ctr packet 'Stop Interference'"



	 
#	if ok  and (interf_data[-1:].isdigit()):
#            if int(interf_data[:1]) == 5 and intr_status == True:
 ##               intr_status = False
  #              print "Interferer ctr packet 'Stop Interference'"

   #         elif int(interf_data[:1]) == 0 and intr_channel != int(interf_data[-1:]):
    #            print "Interferer ctr packet '0'"
#    #            intr_status = True
      #          intr_channel = int(interf_data[-1:])
#      #          uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)

#	    elif int(interf_data[-1:]) == 1 and intr_channel != int(interf_data[-1:]):
 #               print "Interferer ctr packet '0'"
# #               intr_status = True
   #             intr_channel = int(interf_data[-1:])
#   #             uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)

     #       elif int(interf_data[-1:]) == 2 and intr_channel != int(interf_data[-1:]):
      #          print "Interferer ctr packet '2'"
#      #          intr_status = True
        #        intr_channel = int(interf_data[-1:])
#        #        uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)

#            elif int(interf_data[-1:]) == 3 and intr_channel != int(interf_data[-1:]):
 #               print "Interferer ctr packet '3'"
# #               intr_status = True
   #             intr_channel = int(interf_data[-1:])
#   #             uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)

#            elif int(interf_data[-1:]) == 4 and intr_channel != int(interf_data[-1:]):
 #               print "Interferer ctr packet '4'"
# #               intr_status = True
   #             intr_channel = int(interf_data[-1:])
#                uhd_transmitter.set_freq(tb.sink, channels_list[intr_channel], 0)

	
		             
#	if ok  and (data_packet[-1:].isdigit()):
#	    if int(data_packet[-1:]) == 0:
#		print "last item of data is '0'"
#		if cur_channel == 1:
#		    cur_channel = 0
#		    tune_request = True
#		    tune_freq = 1599.25e6
# 	  	    print "Tune request to '0'"
#	    elif int(data_packet[-1:]) == 1:
#		print "last item of data is '1'"
#		if cur_channel == 0:
#		    cur_channel = 1
#		    tune_request = True
#		    tune_freq = 1600.75e6
#		    print "Tune request to '1'"
		
		#print "last item of data is '1'"
	    

#	    omlDb.inject("packets", ("sent", pktno))

        print "ok = %5s  pktno = %4d  n_rcvd = %4d  n_right = %4d" % (
            ok, pktno, n_rcvd, n_right)
	

#	omlDb.inject("packets", ("received", n_rcvd))
#	omlDb.inject("packets", ("correct", n_right))

    demods = digital.modulation_utils.type_1_demods()
    mods = digital.modulation_utils.type_1_mods()    # /---- RX-transmitter ----/

    # Create Options Parser:
    parser = OptionParser (option_class=eng_option, conflict_handler="resolve")
    expert_grp = parser.add_option_group("Expert")

    parser.add_option("-m", "--modulation", type="choice", choices=demods.keys(), 
                      default='gmsk',
                      help="Select modulation from: %s [default=%%default]"
                            % (', '.join(demods.keys()),))
    parser.add_option("","--from-file", default=None,
                      help="input file of samples to demod")
    parser.add_option("-E", "--exp-id", type="string", default="test",
                          help="specify the experiment ID")
    parser.add_option("-N", "--node-id", type="string", default="test",
                          help="specify the experiment ID")
    parser.add_option("","--server", action="store_true", default=False,
                      help="To take data from the server")
    # /------------------------------ RX-transmitter ------------------------------/
    parser.add_option("-s", "--size", type="eng_float", default=1500,
                      help="set packet size [default=%default]")
    parser.add_option("-M", "--megabytes", type="eng_float", default=1.0,
                      help="set megabytes to transmit [default=%default]")
    parser.add_option("","--discontinuous", action="store_true", default=False,
                      help="enable discontinous transmission (bursts of 5 packets)")
    parser.add_option("","--to-file", default=None,
                      help="Output file for modulated samples")
    # /---------------------------------------------------------------------------/


    receive_path.add_options(parser, expert_grp)
    uhd_receiver.add_options(parser)

    transmit_path.add_options(parser, expert_grp)  # /---- RX-transmitter ----/
    uhd_transmitter.add_options(parser)            # /------------------------/

    for mod in demods.values():
        mod.add_options(expert_grp)

    for mod in mods.values():           # /---- RX-transmitter ----/
        mod.add_options(expert_grp)     # /------------------------/

    (options, args) = parser.parse_args ()

    ##################################
    options.rx_freq = channels_list[0]
    options.constellation_points = 4
    options.rx_gain = 38.00
    ##################################

#    omlDb = OMLBase("gnuradiorx",options.exp_id,options.node_id,"tcp:nitlab3.inf.uth.gr:3003")
#    omlDb.addmp("packets", "type:string value:long")
#    omlDb.start()
   
    ########################## Rx-Transmitter channel
    options.tx_freq = channels_list[2]
    options.tx_gain = 50.00
    options.tx_amplitude = 1
    ##########################

 
    if len(args) != 0:
        parser.print_help(sys.stderr)
        sys.exit(1)

    if options.from_file is None:
        if options.rx_freq is None:
            sys.stderr.write("You must specify -f FREQ or --freq FREQ\n")
            parser.print_help(sys.stderr)
            sys.exit(1)


    # connect to server
    if options.server:
    	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    	server_address = ('10.0.1.200', 50001)
    	print >>sys.stderr, 'connecting to %s port %s' % server_address
    	sock.connect(server_address)

    # build the graph
    ########################## modulator value added in top block call ##########################
    tb = my_top_block(mods[options.modulation], demods[options.modulation], rx_callback, options)

    r = gr.enable_realtime_scheduling()
    if r != gr.RT_OK:
        print "Warning: Failed to enable realtime scheduling."

    #cur_channel = new_channel = 0
    #tune_request = False
    tb.start()        # start flow graph
    #thread.start_new_thread(rx_hop, ())

 
    start_time = time.time()
    random_hop_cnt = 0    
    while 1:
        if intr_status == True:
             inter = struct.pack('!H', 1 & 0xffff) #sends interference
             send_pkt(inter)
        elapsed_time = time.time() - start_time

	if (elapsed_time % 1) == 0:
	    print elapsed_time

#	if (time.time() - start_time) > 4 and (time.time() - last_ok_time) > 0.2:
#	    intr_status = False
#	    hop_channel = random_hop_cnt % 5
#	    uhd_receiver.set_freq(tb.source, channels_list[hop_channel], 0)


#	if tune_request == True:
#	    uhd_receiver.set_freq(tb.source, tune_freq, 0)
#	    time.sleep(0.5)
#	    tune_request = False

##################################################

#	    print "rx_freq: " + str(options.rx_freq)
#        if elapsed_time == 10 or elapsed_time == 20 or elapsed_time == 30:
#	if (elapsed_time % 10) == 0:
###	if (elapsed_time % 45) == 0:
	    #tb.source.set_center_freq(uhd.tune_request(1599.25e6, 0))
            #tb.source.set_freq(1599.25e6, 0)
#            uhd_receiver.set_freq(tb.source, 1599.25e6, 0)
#	    time.sleep(0.5)

###            if cur_channel == 0:
##		tb.source.set_center_freq(uhd.tune_request(1600.75e6, 0))
##		tb.source.set_freq(1600.75e6, 0)
###		tune_freq = 1600.75e6
### 	        uhd_receiver.set_freq(tb.source, tune_freq, 0)
###		cur_channel = 1
###		print "hop to 1600.75e6"
###	    elif cur_channel == 1:
		#tb.source.set_center_freq(uhd.tune_request(1599.25e6, 0))
		#tb.source.set_freq(1599.25e6, 0)
###		tune_freq = 1599.25e6
###		uhd_receiver.set_freq(tb.source, tune_freq, 0)
###		cur_channel = 0
###		print "hop to 1599.25e6"
##            tb.source.set_freq(tune_freq, 0)
###	    time.sleep(0.5)
#	    print "rx_freq: " + str(options.rx_freq)

    #tune_freq = 1599.25e6
#    while True:
#        if time.time() > 1 and (time.time() - last_ok_time) > 0.5:
#            if tune_freq == 1599.25e6:
#                tune_freq = 1600.75e6
#            elif tune_freq == 1600.75e6:
#                tune_freq = 1599.25e6
#            uhd_receiver.set_freq(tb.source, tune_freq, 0)

#    while 1:
#    	if tune_request == True:
#	    tune_request = False 
#	    uhd_receiver.set_freq(tb.source, tune_freq, 0)
#	    print "RX freq hopping"
#	    time.sleep(0.5)

    tb.wait()         # wait for it to finish.

    if options.server:
    	sock.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
