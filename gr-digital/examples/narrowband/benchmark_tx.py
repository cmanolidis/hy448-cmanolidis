#!/usr/bin/env python
#
# Copyright 2010,2011,2013 Free Software Foundation, Inc.
# 
# This file is part of GNU Radio
# 
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 
import sys, os

path = os.path.dirname(sys.argv[0]).split("share")[0] + "lib/python2.7/dist-packages"
sys.path.append(path)

os.environ['SHELL'] = "/bin/bash"
os.environ['LC_ALL'] = 'C'
os.environ['LANG'] = 'C'
os.environ['PYTHONPATH'] = os.path.dirname(sys.argv[0]).split("share")[0] +'lib/python2.7/dist-packages'
os.environ['PKG_CONFIG_PATH'] = os.path.dirname(sys.argv[0]).split("share")[0] +'lib/pkgconfig'

from gnuradio import gr
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio.eng_option import eng_option
from optparse import OptionParser

# From gr-digital
from gnuradio import digital

# from current dir
from transmit_path import transmit_path
from uhd_interface import uhd_transmitter

import time, struct, sys
import socket

##### For probe source #######
from gnuradio import uhd     #
from gnuradio import analog  #
##############################

#import os 
#print os.getpid()
#raw_input('Attach and press enter')

class my_top_block(gr.top_block):
    def __init__(self, modulator, options):
        gr.top_block.__init__(self)

        if(options.tx_freq is not None):
            # Work-around to get the modulation's bits_per_symbol
            args = modulator.extract_kwargs_from_options(options)
            symbol_rate = options.bitrate / modulator(**args).bits_per_symbol()

            self.sink = uhd_transmitter(options.args, symbol_rate,
                                        options.samples_per_symbol, options.tx_freq,
                                        options.lo_offset, options.tx_gain,
                                        options.spec, options.antenna,
                                        options.clock_source, options.verbose)
            options.samples_per_symbol = self.sink._sps
            
        elif(options.to_file is not None):
            sys.stderr.write(("Saving samples to '%s'.\n\n" % (options.to_file)))
            self.sink = blocks.file_sink(gr.sizeof_gr_complex, options.to_file)
        else:
            sys.stderr.write("No sink defined, dumping samples to null sink.\n\n")
            self.sink = blocks.null_sink(gr.sizeof_gr_complex)

        # do this after for any adjustments to the options that may
        # occur in the sinks (specifically the UHD sink)
        self.txpath = transmit_path(modulator, options)

        self.connect(self.txpath, self.sink)
        print >> sys.stderr, options



	####### For probe source #########
	alpha = 0.001
        threshold = 30
        self.probe = analog.probe_avg_mag_sqrd_c(threshold,alpha)

	self.source = uhd.usrp_source(
                            ",".join(("", "")),
                            uhd.stream_args(
                            cpu_format="fc32",
                            channels=range(1),
                            ),
            )
	
	self.source.set_samp_rate(333325)
	self.source.set_center_freq(uhd.tune_request(1600.5625e6, 0))
	self.source.set_gain(38.00)

	self.connect(self.source, self.probe)

# /////////////////////////////////////////////////////////////////////////////
#                                   main
# /////////////////////////////////////////////////////////////////////////////

def main():
    global tb, channels_list

    def send_pkt(payload='', eof=False):
        return tb.txpath.send_pkt(payload, eof)

    def energy_detection(duration):
	entry_time = time.time()
	probe_sum = 0
	counter = 0
	if duration == -1:
	    return tb.probe.level()
	else:
	    while (time.time() - entry_time) < duration:
	        probe_sum += tb.probe.level()
	        counter += 1
	    return probe_sum/counter

    mods = digital.modulation_utils.type_1_mods()

    parser = OptionParser(option_class=eng_option, conflict_handler="resolve")
    expert_grp = parser.add_option_group("Expert")

    parser.add_option("-m", "--modulation", type="choice", choices=mods.keys(),
                      default='gmsk',
                      help="Select modulation from: %s [default=%%default]"
                            % (', '.join(mods.keys()),))

    parser.add_option("-s", "--size", type="eng_float", default=1500,
                      help="set packet size [default=%default]")
    parser.add_option("-M", "--megabytes", type="eng_float", default=1.0,
                      help="set megabytes to transmit [default=%default]")
    parser.add_option("","--discontinuous", action="store_true", default=False,
                      help="enable discontinous transmission (bursts of 5 packets)")
    parser.add_option("","--from-file", default=None,
                      help="use intput file for packet contents")
    parser.add_option("","--to-file", default=None,
                      help="Output file for modulated samples")
    parser.add_option("","--server", action="store_true", default=False,
                      help="To take data from the server")

    transmit_path.add_options(parser, expert_grp)
    uhd_transmitter.add_options(parser)

    for mod in mods.values():
        mod.add_options(expert_grp)

    (options, args) = parser.parse_args ()

    ########### List of avaliable pre-determined channels ############
    channels_list = [1598.875e6, 1599.4375e6 , 1600.0e6, 1600.5625e6, 1601.125e6]
    ##################################################################

    ################ Transmit-Interfere Thresholds ################
    transmit_thres = 5.86e-6
    interf_thres = 3.8e-6
    ###############################################################

#Back to commit 19f20a3
    ##################################
    options.tx_freq = channels_list[0]
    options.constellation_points = 4
    options.bitrate = 333325
    
    options.tx_gain = 87.00
    options.tx_amplitude = 1.00
    ##################################

    if len(args) != 0:
        parser.print_help()
        sys.exit(1)
           
    if options.from_file is not None:
        source_file = open(options.from_file, 'r')

    # build the graph
    tb = my_top_block(mods[options.modulation], options)

    r = gr.enable_realtime_scheduling()
    if r != gr.RT_OK:
        print "Warning: failed to enable realtime scheduling"

    tb.start()                       # start flow graph
        
    # generate and send packets
    nbytes = int(1e6 * options.megabytes)
    n = 0
    pktno = 0
    pkt_size = int(options.size)

    #### Probe #######
    probe_side = 0
    hops_no = 0
#    while 1:
#		if probe_side == 0:
#                    tb.source.set_center_freq(uhd.tune_request(1600.75e6, 0))
#		    probe_side = 1
#                elif probe_side == 1:
#		    tb.source.set_center_freq(uhd.tune_request(1599.25e6, 0))
#		    probe_side = 0
#		tune_time = time.time()
		
#		print "No of hops: ", hops_no
#		hops_no += 1
#		print "Channel : ", probe_side
#		probe_sum = energy_detection(0.1)
#               print "Probe value 1: ", probe_sum
#		time.sleep(0.4)		

##                while ( time.time() - tune_time) < 3:
##		    if ( time.time() - tune_time) == 1:

#		probe_sum = energy_detection(0.1)
#              	print "Probe value 2: ", probe_sum
#		time.sleep(0.4)

##		   if ( time.time() - tune_time) == 2:
		
#		probe_sum = energy_detection(0.1)
#                print "Probe value 3: ", probe_sum
#		time.sleep(0.4)
		
#		probe_sum = energy_detection(0.1)
#               print "Probe value 4: ", probe_sum
#		time.sleep(0.4)

##		    print_time = time.time()
##		    while ( print_time - time.time() ) < 1:
##                        probe_sum = energy_detection(-1)
##                        print "Probe value: ", probe_sum
##		        time_captured = time.time()
##		        print "Capture time: ", time_captured
    #################

    # connect to server
    if options.server:
    	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_address = ('10.0.1.200', 50000)
    	print >>sys.stderr, 'connecting to %s port %s' % server_address
    	sock.connect(server_address)

    packetList = []
    while n < nbytes or options.server:
	if pktno > 1000:                # Recieve only first 1000 packets
            break;
        if options.server:
            data = "";
            while len(data) < pkt_size:
                data += sock.recv(pkt_size - len(data))
                if data == '':
                    # No more data received from server
                    sock.close()
                    break;
        elif options.from_file is None:
            data = (pkt_size - 2) * chr(pktno & 0xff)
        else:
            data = source_file.read(pkt_size - 2)
            if data == '':
                break;

	packetList.append(data)
	pktno +=1

    probe_sum = 0
    cnl = 0
    while 1:
	pktno = 0	
	for pck in packetList:
	    #data_packet = pck + str(cnl)
#	    if pktno == 499:
#		cnl = 1
#		tune_freq = 1600.75e6
#		data_packet = pck + str(cnl)
#                payload = struct.pack('!H', pktno & 0xffff) + data_packet
#                send_pkt(payload)
#		send_pkt(payload)
#		send_pkt(payload)
#		send_pkt(payload)
#            if pktno == 999:
#                cnl = 0
#                tune_freq = 1599.25e6
#	 	data_packet = pck + str(cnl)
#                payload = struct.pack('!H', pktno & 0xffff) + data_packet
#                send_pkt(payload)
#		send_pkt(payload)
#		send_pkt(payload)
#		send_pkt(payload)
#	    data_packet = pck + str(cnl)
#            payload = struct.pack('!H', pktno & 0xffff) + data_packet
#            send_pkt(payload)

#            if pktno == 999 or pktno == 499:
		#send_pkt(payload)
                #if first_time == False:
#		time.sleep(1)
#	        uhd_transmitter.set_freq(tb.sink, tune_freq, 0)
#	        time.sleep(5)




#            if pktno == 500:
#                sense_start_time = time.time()
            #if pktno < 500:
            #    data_packet = pck + str(-1)
            #    payload = struct.pack('!H', pktno & 0xffff) + data_packet
            #    send_pkt(payload)
	#	print "Send Packet"
#            if pktno == 360:
#                print "pktno: " + str(pktno) + "Probe value: " + str(energy_detection(0.05))
#	    if pktno < 500:
#		data_packet = pck + str(0)
#                payload = struct.pack('!H', pktno & 0xffff) + data_packet
#                send_pkt(payload)
#            elif pktno >= 500:
#		data_packet = pck + str(-1)
#                payload = struct.pack('!H', pktno & 0xffff) + data_packet
#                send_pkt(payload)
#		print "Send Packet"		

                #if ((time.time() - sense_start_time) % 0.05 == 0):
                    #print "Elaplsed time after stopping transmittion: " + time.time() - sense_start_time
#                print "Elaplsed time after stopping transmittion: " + str(time.time() - sense_start_time)
#                print "pktno: " + str(pktno) + "Probe value: " + str(energy_detection(0.05))
##              print "Hello World!!"	    	

	    data_packet = pck + str(pktno % 10)
	    payload = struct.pack('!H', pktno & 0xffff) + data_packet
 	    send_pkt(payload)

#	    n += len(payload)
            sys.stderr.write('.')
            if options.discontinuous and pktno % 5 == 4:
                time.sleep(1)
            pktno += 1
	
#    print "Probe sum: ", probe_sum / 1000
        
    send_pkt(eof=True)

    tb.wait()                       # wait for it to finish


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
